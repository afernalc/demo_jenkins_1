# README #

Repositorio  para una demo básica de Integración continua desde Jenkins

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### JMeter ###

* Se genera de un Dockerfile segui se indica en esta url de blazemeter https://www.blazemeter.com/blog/make-use-of-docker-with-jmeter-learn-how/
* Para generar la imagen, crear en un directorio "testing" los ficheros Dockerfile y launch.sh y generarla ejecutando desde ese directorio el comando: docker image build -t my_jmeter .
*D:\REPOS\demo_jenkins_1\testing>docker container cp D:\Alfonso\AAAProyectos\Demostrador_Tecnologico_Devops\scripts_jmeter\petClinic_REST_v6_basico.jmx 3584b3c5ee18:/tmp

D:\REPOS\demo_jenkins_1\testing>docker container exec -it 3584b3c5ee18  /bin/sh

Construir con: docker image build -t my_jmeter .


### Direccion repositorio ###
https://afernalc@bitbucket.org/afernalc/demo_jenkins_1.git

https://www.liatrio.com/blog/building-with-docker-using-jenkins-pipelines

### ENLACES UTILES ###
- Jmeter con Docker
https://www.blazemeter.com/blog/make-use-of-docker-with-jmeter-learn-how/

- Construir con Docker en Pipelines Jenkins
https://www.liatrio.com/blog/building-with-docker-using-jenkins-pipelines

- Petclinic
https://github.com/spring-petclinic/spring-petclinic-rest
   
### Comandos Docker ###
-- Arrnacar la aplicacion
docker container run -d -p 9966:9966 --rm afalcalde/petclinic-rest

-- COMPOSE
  -- Arrancar
  docker-compose up

-- Contenedores
  docker container stop c41be886cf1b
  docker run -p 8080:8080 -p 9966:9966 --rm jenkins2
  docker run -it my_jmeter
  
  -- Conecarse a un contenedor en ejecución
  docker container exec -it 3584b3c5ee18 /bin/sh
  
-- Construir imagen con dockerfile directorio actual
docker image build -t jenkins2 .


-- Ejecutar imagen
docker run -p 8080:8080 -p 9966:9966 --rm jenkins2
-- Eliminar imagen
docker image rm jenkins2 -f
docker image prune
  


